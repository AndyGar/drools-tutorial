//created on: 24-ene-2018
package cours

//list any import classes here.
import droolscours.CashFlow;
import util.OutputDisplay;
import droolscours.PrivateAccount;
import droolscours.Customer;
import droolscours.Account;
import droolscours.service.CustomerService;
import java.util.ArrayList;
import droolscours.AccountingPeriod;


//declare any global variables here
global OutputDisplay showResult;
global CustomerService serviceCustomer;

/*
rule "The cashFlow can be credit or debit"

    when
    	$cash: CashFlow(type in (CashFlow.DEBIT, CashFlow.CREDIT))
    then
    	showResult.showText("The cashFlow is a credit or a debit");

end
*/

rule "Accessor"
    when
        $cash :PrivateAccount( owner.name == "Heron" )
    then
        showResult.showText("Account is owned by Heron");
end

rule "infixAnd"
	when
		( $c1: Customer ( country == "GB" ) and PrivateAccount ( owner == $c1 ))
			or
		( $c1: Customer (country == "US" ) and PrivateAccount (owner == $c1 ))
	then
		showResult.showText("Person lives in GB or US");
end

rule "no customer"
	when
		not Customer()
	then
		showResult.showText("No customer");
end

rule "Exists"
	when
		exists Account()
	then
		showResult.showText("Account exists");
end

// Para todos las cuentas existe un cashflow asociado a esa cuenta (todas las cuentas tienen algun cashflow)
rule "ForAll"
	when
		forall (Account($no: accountNo)
					CashFlow( accountNo == $no )
				)
	then
		showResult.showText("All cashflows are related to an Account");
end

// Comprueba si existen 2 customers con el mismo country. Usamos el from para importar datos de fuera de la sesion
rule "FromCondition"
	when
		$c: Customer()
		$cc: Customer(name == $c.name, surname == $c.surname, country != $c.country) from serviceCustomer.getListCustomer();
	then
		showResult.showText("Found same customer in 2 countries!");
end

// Devuelve una lista (collect) de todos los cashflow cuya fecha esta entre el accountingperiod y su cuenta es Account y haya 2 o mas
rule "More than 2 CashFlow Line"
	when
		$c: Account ( $acc: accountNo )
		$p: AccountingPeriod ( $sDate: startDate, $eDate: endDate )
		$number: ArrayList (size >= 2)
			from collect( CashFlow ( mvtDate >= $sDate && mvtDate <= $eDate, accountNo == $acc ))
	then
		showResult.showText("Found more than 2 CashFlow Lines");
		showResult.showText("<<<<<<<<<");
		for (Object ff: $number){
			showResult.showText(ff.toString());
		}
		showResult.showText(">>>>>>>>");
end

// Igual pero da igual cuantos CashFlow hay
rule "Numbers of CashFlow Line"
	when
		$c: Account ($acc: accountNo)
		$p: AccountingPeriod ($sDate: startDate, $eDate: endDate)
		$number: ArrayList ()
			from collect(CashFlow (mvtDate >= $sDate && mvtDate <= $eDate, accountNo == $acc ))
	then
		showResult.showText("Found "+$number+" more than 2 CashFlow Lines");
end

/******** Collect from
* Nos permite generar un sumando en una variable
* Toma 5 parametros: 1. Una restriccion de hechos 2. Una condicion incial
* 3. La accion a realizar para cuando la restriccion de hechos se cumplen 
* 4. La accion inersa para cuando no se cumple la restriccion 5. El resultado del acumulador
********/

rule "Credit and Debit rule"
	when
		$c: Account ($acc: accountNo)
		$p: AccountingPeriod ($sDate: startDate, $eDate: endDate)
		$totalCredit: Number (doubleValue > 100)
			from accumulate (CashFlow (type == CashFlow.CREDIT, $value: amount, mvtDate >= $sDate && mvtDate <= $eDate, accountNo == $acc),
							init (double total = 0; ),
							action (total += $value; ),
							reverse (total -= $value; ),
							result (total))
		$totalDebit: Number(doubleValue > 100)
			from accumulate (CashFlow (type == CashFlow.DEBIT, $value: amount, mvtDate >= $sDate && mvtDate <= $eDate, accountNo == $acc),
							init (double total = 0; ),
							action(total += $value; ),
							reverse(total -= $value; ),
							result(total))
	then
		showResult.showText("Found "+$totalCredit+" as a credit");
		showResult.showText("Found "+$totalDebit+" as a debit");
end
		
						
	