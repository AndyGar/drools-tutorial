package util;

import java.text.SimpleDateFormat;
import java.util.Date;

public class DateHelper {

	public static String sFormat = "yyyy-MM-dd";
	
	public static Date getDate(String sDate) throws Exception {
		SimpleDateFormat sdf = new SimpleDateFormat(sFormat);
		return sdf.parse(sDate);
	}
	
	public static Date getDate(String sDate, String anotherformat) throws Exception {
		SimpleDateFormat sdf = new SimpleDateFormat(anotherformat);
		return sdf.parse(sDate);
	}
	
}
